# Learning Wepack

## Objective

Understand how weback is incorporated and configured in a front end project to transform the source code, and generate a build which could be used for development and deployment.

## User Story 1

Create the minimum config needed to run webpack against a very simple javascript program.

### What Did I Do?

- If you use the recommended webpack project structure (e.g. src folder with an index.js file) no explicit webpack config is needed.
- I added webpack as a dev dependency and created a very simple javascript programme with one function in index.js which is executed immediately.
- I then ran the build by creating a script in package.json to run `webpack`. This could also be done using `npx webpack` from the command line.
- This doesn't do much as there's no html file and no web server, but you can see the transpiled javascript being created.

## User Story 2

- Create an html file alongside the javascript automatically by webpack.
- Add the minimum config to generate an html file in the build folder alongside the transpiled javascript, using the HtmlWebpackPlugin.

### What Did I Do?

- Create a `webpack.config.js` file in the project root as we now need to specify some config.
- Install the html-webpack-plugin npm package as a dev dependencies.
- Add a section in the config to use the plugin, without any options.
- Run `npm run build` again, and notice that an html file is generated in the build.

## How to Install

### Clone the Project

Use git clone command.

### Install Dependencies

```console
$ npm i
```

### How to Run

```console
npm run build
```

## Resources

https://webpack.js.org/guides/development/
